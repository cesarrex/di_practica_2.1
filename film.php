<?php
include "class/Film.php";

//Json
$film_id              = $_POST['film_id'] ?? null;
$title                = $_POST['title'] ?? null;
$description          = $_POST['description'] ?? null;
$release_year         = $_POST['release_year'] ?? null;
$language_id          = $_POST['language_id'] ?? null;
$original_language_id = $_POST['original_language_id'] ?? null;
$rental_duration      = $_POST['rental_duration'] ?? null;
$rental_rate          = $_POST['rental_rate'] ?? null;
$length               = $_POST['length'] ?? null;
$replacement_cost     = $_POST['replacement_cost'] ?? null;
$rating               = $_POST['rating'] ?? null;
$special_features     = $_POST['special_features'] ?? null;
$image                = $_POST['image'] ?? null;
$last_update          = $_POST['last_update'] ?? null;

$page                 = $_POST['page'] ?? 1;
$rows_per_page        = $_POST['rows_per_page'] ?? 20;

// Ponemos response a true
$response = ["success" => true];
try {

    switch ($_POST["action"]) {
        case 'listado':
            
            $response["msg"] = "Listado de las Peliculas.";
            $instanciaPelicula = Film::list($rows_per_page, $page);
            $response["data"] =  $instanciaPelicula;
            $response["rows_per_page"]=$rows_per_page;
            $response["page"]=$page;
            $response["num_rows"]=Film::num_rows();
        break;

        case 'insert':
            $response["msg"] = "Insertar.";
            $instanciaPelicula = new Film(
                null, $title, $description, $release_year, $language_id,
                $original_language_id, $rental_duration, $rental_rate, $length,
                $replacement_cost, $rating, $special_features, $image, $last_update
            );
            $instanciaPelicula->insert();
        break;

        case 'delete':
            $response["msg"] = "Eliminar.";
            $instanciaPelicula = new Film($film_id);
            $instanciaPelicula->delete();
        break;

        case 'update':
            $response["msg"] = "Actualizar.";
            $instanciaPelicula = new Film(
                $film_id, $title, $description, $release_year, $language_id,
                $original_language_id, $rental_duration, $rental_rate, $length,
                $replacement_cost, $rating, $special_features, $image, $last_update
            );
            $instanciaPelicula->update();
            $response["data"] = $instanciaPelicula;
        break;

        case 'select':
            $response["msg"] = "Seleccionar.";
            $response["data"] = Film::select($film_id);
        break;

        case 'num_rows':
            $response["msg"] = "Num rows.";
            $response["num_rows"] = Film::num_rows();
        break;

        default:
            $response["msg"] = "Campo no válido.";
        break;
    }

} catch (Exception $e) {
    $response["success"] = false;
    $response["error"] = $e->getMessage();
}

// Hacer saber al header el tipo de dato que estamos enviando
header("Content-Type: application/json");
echo json_encode($response);