$(document).ready(function () {
    num_rows();
});
function emergenteEliminar($film_id){
    var r = confirm("¿Estás seguro de eliminar "+$film_id+"?");
    if (r==true) {
        console.log("Eliminar: "+$film_id);
        eliminar($film_id)
    }
}
function num_rows() {
    $.ajax({
        method: "POST",
        url: "./film.php",
        data: {
            action: "num_rows",
        },
        dataType: "json"
    })
        // en caso de éxito
        .done(function (response) {
            console.log(response);
            $("#num_rowsMostrar").remove();
            $("#num_rows").append("<label id='num_rowsMostrar' valor='"+response.num_rows+"'>Número total de registros: "+response.num_rows+"</label>");
        })
        // En caso de fallo
        .fail(function (jqXHR, textStatus, errorThrown) {
            alert("Error: " + textStatus);
        });
}
//Listado de peliculas
function lista(boton) {
    event.preventDefault();
    //Botones
    num_rows=$("#num_rowsMostrar").attr('valor');
    pageForm=$("#page").val();
    //Label con los registros totales
    rows_per_pageForm=$('#rows_per_page').val();
    parseInt(rows_per_pageForm);
    parseInt(num_rows);
    paginas=Math.ceil(num_rows/rows_per_pageForm);
    //Botones de posicion
    if (typeof boton != "undefined") {
        if (boton==("primero")) {
            pageForm = 1;
        }else if (boton==("siguiente") && pageForm<paginas) {
            pageForm++;
        }else if (boton==("anterior") && pageForm>1) {
            pageForm--;
        }else if (boton==("ultimo")) {
            pageForm=paginas;
        }
    }
    //console.log("Num_rows: "+num_rows+" Rows per page: "+rows_per_pageForm);

    $.ajax({
        method: "POST",
        url: "./film.php",
        data: {
            action: "listado",
            page: pageForm,
            rows_per_page: rows_per_pageForm,
        },
        dataType: "json"
    })
        // en caso de éxito
        .done(function (response) {
            // Escribe el mensaje recibido en el JSON descartando el contenido anterior
            $(".rowsFilms").remove();
            for (var i = 0; i < response.data.length; i++) {
                console.log(response.data[i].film_id);
                $('#contenido').append('<tr class="rowsFilms"><td>' + response.data[i].film_id + '</td><td>'
                    + response.data[i].title + '</td><td>' + response.data[i].description + '</td><td>'
                    + response.data[i].release_year + '</td><td>' + response.data[i].language_id + '</td><td>'
                    + response.data[i].original_language_id + '</td><td>' + response.data[i].rental_duration + '</td><td>'
                    + response.data[i].rental_rate + '</td><td>' + response.data[i].length + '</td><td>'
                    + response.data[i].replacement_cost + '</td><td>' + response.data[i].rating + '</td><td>'
                    + response.data[i].special_features + '</td><td>' + response.data[i].image + '</td><td>'
                    + response.data[i].last_update + '</td><td><button class="btn btn-danger" onclick="emergenteEliminar(' + response.data[i].film_id + ')">'+
                    'Delete</button></td><td><button class="btn btn-secondary" onclick="select(' + response.data[i].film_id + ')">Edit</button></td></tr>');
            }
            $("#page").val(response.page);
            $("#rows_per_pageForm").val(response.rows_per_page);
        })
        // En caso de fallo
        .fail(function (jqXHR, textStatus, errorThrown) {
            alert("Error: " + textStatus);
        });
}
//Eliminar
function eliminar($film_id) {
    $.ajax({
        method: "POST",
        url: "./film.php",
        data: {
            action: "delete",
            film_id: $film_id,
        },
        dataType: "json"
    })
        // en caso de éxito
        .done(function (response) {
            // Escribe el mensaje recibido en el JSON descartando el contenido anterior
            console.log(response);
            
        })
        // En caso de fallo
        .fail(function (jqXHR, textStatus, errorThrown) {
            alert("Error: " + textStatus);
        });
}
//Formulario para editar peliculas
function select($film_id) {
    $.ajax({
        method: "POST",
        url: "./film.php",
        data: {
            action: "select",
            film_id: $film_id,
        },
        dataType: "json"
    })
        // en caso de éxito
        .done(function (response) {
            // Escribe el mensaje recibido en el JSON descartando el contenido anterior
            console.log(response.data.film_id);
            $("#datosFilm").remove();
            $("#botonesModalEdit").remove();
            $('#formEdit').append('<div id="datosFilm"><div class="form-group"><label>Film ID</label><input name="film_id" type="text" class="form-control" readonly value="' + response.data[0].film_id + '" /></div>' +
                '<div class="form-group"><label>Title</label><input name="title" type="text" class="form-control" value="' + response.data[0].title + '" /></div>' +
                '<div class="form-group"><label>Description</label><input name="description" type="text" class="form-control" value="' + response.data[0].description + '" /></div>' +
                '<div class="form-group"><label>Release Year</label><input name="release_year" type="text" class="form-control" value="' + response.data[0].release_year + '" /></div>' +
                '<div class="form-group"><label>Language ID</label><input name="language_id" type="text" class="form-control" value="' + response.data[0].language_id + '" /></div>' +
                '<div class="form-group"><label>Original Language</label><input name="original_language_id" type="text" class="form-control" value="' + response.data[0].original_language_id + '" /></div>' +
                '<div class="form-group"><label>Rental Duration</label><input name="rental_duration" type="text" class="form-control" value="' + response.data[0].rental_duration + '" /></div>' +
                '<div class="form-group"><label>Rental Rate</label><input name="rental_rate" type="text" class="form-control" value="' + response.data[0].rental_rate + '" /></div>' +
                '<div class="form-group"><label>Length</label><input name="length" type="text" class="form-control" value="' + response.data[0].length + '" /></div>' +
                '<div class="form-group"><label>Replacement Cost</label><input name="replacement_cost" type="text" class="form-control" value="' + response.data[0].replacement_cost + '" /></div>' +
                '<div class="form-group"><label>Rating</label><input name="rating" type="text" class="form-control" value="' + response.data[0].rating + '" /></div>' +
                '<div class="form-group"><label>Special Features</label><input name="special_features" type="text" class="form-control" value="' + response.data[0].special_features + '" /></div>' +
                '<div class="form-group"><label>Image</label><input name="image" type="text" class="form-control" value="' + response.data[0].image + '" /></div>' +
                '<div class="form-group"><label>Last Update</label><input name="last_update" type="text" class="form-control" value="' + response.data[0].last_update + '" /></div></div>');
            $('#modalFooterEdit').append('<div id="botonesModalEdit"><button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>' +
                '<button type="submit" form="formEdit" onclick="editar()" class="btn btn-primary">Save changes</button></div>');
            $("#getCode").html(response);
            $("#edit").modal('show');

        })
        // En caso de fallo
        .fail(function (jqXHR, textStatus, errorThrown) {
            alert("Error: " + textStatus);
        });
}
//Formulario para insertar peliculas
function formulario() {
    // Escribe el mensaje recibido en el JSON descartando el contenido anterior
    $("#datosFilm").remove();
    $("#botonesModalEdit").remove();
    $('#formEdit').append('<div id="datosFilm"><div class="form-group"><label>Title</label><input name="title" type="text" class="form-control" value="" /></div>' +
        '<div class="form-group"><label>Description</label><input name="description" type="text" class="form-control" value="" /></div>' +
        '<div class="form-group"><label>Release Year</label><input name="release_year" type="text" class="form-control" value="" /></div>' +
        '<div class="form-group"><label>Language ID</label><input name="language_id" type="text" class="form-control" value="" /></div>' +
        '<div class="form-group"><label>Original Language</label><input name="original_language_id" type="text" class="form-control" value="" /></div>' +
        '<div class="form-group"><label>Rental Duration</label><input name="rental_duration" type="text" class="form-control" value="" /></div>' +
        '<div class="form-group"><label>Rental Rate</label><input name="rental_rate" type="text" class="form-control" value="" /></div>' +
        '<div class="form-group"><label>Length</label><input name="length" type="text" class="form-control" value="" /></div>' +
        '<div class="form-group"><label>Replacement Cost</label><input name="replacement_cost" type="text" class="form-control" value="" /></div>' +
        '<div class="form-group"><label>Rating</label><input name="rating" type="text" class="form-control" value="" /></div>' +
        '<div class="form-group"><label>Special Features</label><input name="special_features" type="text" class="form-control" value="" /></div>' +
        '<div class="form-group"><label>Image</label><input name="image" type="text" class="form-control" value="" /></div>' +
        '<div class="form-group"><label>Last Update</label><input name="last_update" type="text" class="form-control" value="" /></div></div>');
    $('#modalFooterEdit').append('<div id="botonesModalEdit"><button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>' +
        '<button type="submit" form="formEdit" onclick="insertar()" class="btn btn-primary">Insert</button></div>');
    $("#edit").modal('show');

}
//Editar pelicula
function editar() {
    //event.preventDefault();
    $.ajax({
        method: "POST",
        url: "./film.php",
        data: {
            action: "update",
            film_id: $('input[name=film_id]').val(),
            title: $('input[name=title]').val(),
            description: $('input[name=description]').val(),
            release_year: $('input[name=release_year]').val(),
            language_id: $('input[name=language_id]').val(),
            original_language_id: $('input[name=original_language_id]').val(),
            rental_duration: $('input[name=rental_duration]').val(),
            rental_rate: $('input[name=rental_rate]').val(),
            length: $('input[name=length]').val(),
            replacement_cost: $('input[name=replacement_cost]').val(),
            rating: $('input[name=rating]').val(),
            special_features: $('input[name=special_features]').val(),
            image: $('input[name=image]').val(),
            last_update: $('input[name=last_update]').val(),
        },
        dataType: "json"
    })
        // en caso de éxito
        .done(function (response) {
            // Escribe el mensaje recibido en el JSON descartando el contenido anterior
            console.log("Response de Edit"+response);

        })
        // En caso de fallo
        .fail(function (jqXHR, textStatus, errorThrown) {
            alert("Error: " + textStatus);
        });
}
//Insertar pelicula
function insertar() {
    //event.preventDefault();
    $.ajax({
        method: "POST",
        url: "./film.php",
        data: {
            action: "insert",
            film_id: $('input[name=film_id]').val(),
            title: $('input[name=title]').val(),
            description: $('input[name=description]').val(),
            release_year: $('input[name=release_year]').val(),
            language_id: $('input[name=language_id]').val(),
            original_language_id: $('input[name=original_language_id]').val(),
            rental_duration: $('input[name=rental_duration]').val(),
            rental_rate: $('input[name=rental_rate]').val(),
            length: $('input[name=length]').val(),
            replacement_cost: $('input[name=replacement_cost]').val(),
            rating: $('input[name=rating]').val(),
            special_features: $('input[name=special_features]').val(),
            image: $('input[name=image]').val(),
            last_update: $('input[name=last_update]').val(),
        },
        dataType: "json"
    })
        // en caso de éxito
        .done(function (response) {
            // Escribe el mensaje recibido en el JSON descartando el contenido anterior
            console.log("Response de Edit".response);

        })
        // En caso de fallo
        .fail(function (jqXHR, textStatus, errorThrown) {
            alert("Error: " + textStatus);
        });
}



