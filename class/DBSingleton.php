<?php
require_once __DIR__."/../inc/conf.inc.php";
class Singleton {
  private static $instance = null;
  private $conn;

   
  private function __construct(){
    $this->conn = new PDO("mysql:host=".DB_HOST.";
    dbname=".DB_NAME, DB_USER,DB_PASS,
    array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));
  }
  
  public static function getInstance(){
    if(!self::$instance) {
      self::$instance = new Singleton();
    }
   
    return self::$instance;
  }
  
  public function getConnection(){
    return $this->conn;
  }
}
?>