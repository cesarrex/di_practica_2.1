<?php 
require_once "DBSingleton.php";
class Film {
	//Atributos
	private $film_id;
	private $title;
	private $description;
	private $release_year;
	private $language_id;
	private $original_language;
	private $rental_duration;
 	private $rental_rate;
 	private $length;
 	private $replacement_cost;
 	private $rating;
 	private $special_features;
 	private $image;
	private $last_update;
	 
	 function __construct($film_id=null, $title=null, $description=null, $release_year=null,
	 $language_id=null,$original_language=null, $rental_duration=null, $rental_rate=null,
	 $length=null, $replacement_cost=null, $rating=null, $special_features=null, $image=null, $last_update=null){
 		$this->setFilm_id($film_id);
 		$this->setTitle($title);
 		$this->setDescription($description);
 		$this->setRealease_year($release_year);
 		$this->setLanguage_id($language_id);
 		$this->setOriginal_language($original_language);
 		$this->setRental_duration($rental_duration);
 		$this->setRental_rate($rental_rate);
 		$this->setLength($length);
 		$this->setReplacement_cost($replacement_cost);
 		$this->setRating($rating);
 		$this->setSpecial_features($special_features);
 		$this->setImage($image);
 		$this->setLast_update($last_update);
 	}
 	//Destruct
 	function __destruct(){

 	}
 	//Getters y setters
 	public function getFilm_id(){
 		return $this->film_id;
 	}
 	public function setFilm_id($film_id){
		 if (strlen((string)$film_id)<=5){
			$this->film_id=$film_id;
		 }
 	}

 	public function getTitle(){
 		return $this->title;
 	}
 	public function setTitle($title){
		 if (strlen($title)<=255) {
			$this->title=$title;
		 }
 	}

 	public function getDescription(){
 		return $this->description;
 	}
 	public function setDescription($description){
 		$this->description=$description;
 	}

 	public function getRelease_year(){
 		return $this->release_year;
 	}
 	public function setRealease_year($release_year){
		$this->release_year=$release_year;
 	}

 	public function getLanguage_id(){
 		return $this->language_id;
 	}
 	public function setLanguage_id($language_id){
		 if(strlen((string)$language_id)<=3){
			$this->language_id=$language_id;
		 }
 	}

 	public function getOriginal_language(){
 		return $this->original_language;
 	}
 	public function setOriginal_language($original_language){
		if(strlen((string)$original_language)<=3){ 
			$this->original_language=$original_language;
		}
 	}

 	public function getRental_duration(){
 		return $this->rental_duration;
 	}
 	public function setRental_duration($rental_duration){
		if(strlen((string)$rental_duration)<=3){
			$this->rental_duration=$rental_duration;
		} 
 	}

 	public function getRental_rate(){
 		return $this->rental_rate;
 	}
 	public function setRental_rate($rental_rate){
		 if(strlen((string)$rental_rate)<=4.2){
			$this->rental_rate=$rental_rate;
		 }
 	}

 	public function getLength(){
 		return $this->length;
 	}
 	public function setLength($length){
		if (strlen((string)$length)<=5) {
			$this->length=$length;
		}
 	}

 	public function getReplacement_cost(){
 		return $this->replacement_cost;
 	}
 	public function setReplacement_cost($replacement_cost){
		if(strlen((string)$replacement_cost)<=5.2){
			$this->replacement_cost=$replacement_cost;
		} 
 	}

 	public function getRating(){
 		return $this->rating;
 	}
 	public function setRating($rating){
		$arrayRatings = array("G", "PG", "PG-13", "R", "NC-17");
		foreach ($arrayRatings as $valor){
			if(strcmp($rating, $valor)==0){
				$this->rating=$rating;
			}
		}
 	}

 	public function getSpecial_features(){
		return $this->special_features;
 	}
 	public function setSpecial_features($special_features){
		$arraySpecial_features = array("Trailers", "Commentaries", "Deleted Scenes", "Behind the Scenes");
		foreach ($arraySpecial_features as $valor){
			if (strcmp($special_features, $valor)==0){
				$this->special_features=$special_features;
			}
		} 
 	}

 	public function getImage(){
 		return $this->image;
 	}
 	public function setImage($image){
		 if ((strlen($image))<=250){
			$this->image=$image;
		 }
 	}

 	public function getLast_update(){
 		return $this->last_update;
 	}
 	public function setLast_update($last_update){
 		$this->last_update=$last_update;
	 }
	//toString
	public function __toString(){
		return " ID de Pelicula: ".$this->$film_id." Titulo de la Pelicula: " .$this->$title. " Descripcion: ".$this->$description." Fecha de estreno: ".$this->$release_year." Lenguaje: ".$this->$language_id." Idioma VOSE: ".$this->$original_language.
		" Duración de renta ".$this->$rental_duration." Cantidad de rentas: ".$this->$rental_rate." Duración: ".$this->$length." Precio de remplazo: ".$this->$replacement_cost." Ratio: ".$this->$rating." Extras: ".$this->$special_features." Imagen: ".$this->$image." Ultima actualización:  ".$this->$last_update;
	}
	//Insert
	public function insert(){
		$parametro=array();

		$cc=Singleton::getInstance();
        $sql="INSERT INTO film (title, description, release_year, language_id, original_language_id, rental_duration, rental_rate, length, replacement_cost, rating, special_features, image, last_update)
		VALUES(:title, :description, :release_year, :language_id, :original_language, :rental_duration, :rental_rate, :length, :replacement_cost, :rating, :special_features, :image, :last_update)";
		
		$parametro[":title"]=$this->getTitle();
		$parametro[":description"]=$this->getDescription();
		$parametro[":release_year"]=$this->getRelease_year();
		$parametro[":language_id"]=$this->getLanguage_id();
		$parametro[":original_language"]=$this->getOriginal_language();
		$parametro[":rental_duration"]=$this->getRental_duration();
		$parametro[":rental_rate"]=$this->getRental_rate();
		$parametro[":length"]=$this->getLength();
		$parametro[":replacement_cost"]=$this->getReplacement_cost();
		$parametro[":rating"]=$this->getRating();
		$parametro[":special_features"]=$this->getSpecial_features();
		$parametro[":image"]=$this->getImage();
		$parametro[":last_update"]=$this->getLast_update();
		
		$stmt=$cc->getConnection()->prepare($sql);
		$stmt->execute($parametro);
	}
	public function update(){
		$parametro=array();
		$cc=Singleton::getInstance();
		$sql="UPDATE film SET title=:title, description=:description, release_year=:release_year, language_id=:language_id, original_language_id=:original_language,
		rental_duration=:rental_duration, rental_rate=:rental_rate, length=:length, replacement_cost=:replacement_cost, rating=:rating, special_features=:special_features,
		image=:image, last_update=:last_update WHERE film_id=:film_id";
		
		$parametro[":film_id"]=$this->getFilm_id();
		$parametro[":title"]=$this->getTitle();
		$parametro[":description"]=$this->getDescription();
		$parametro[":release_year"]=$this->getRelease_year();
		$parametro[":language_id"]=$this->getLanguage_id();
		$parametro[":original_language"]=$this->getOriginal_language();
		$parametro[":rental_duration"]=$this->getRental_duration();
		$parametro[":rental_rate"]=$this->getRental_rate();
		$parametro[":length"]=$this->getLength();
		$parametro[":replacement_cost"]=$this->getReplacement_cost();
		$parametro[":rating"]=$this->getRating();
		$parametro[":special_features"]=$this->getSpecial_features();
		$parametro[":image"]=$this->getImage();
		$parametro[":last_update"]=$this->getLast_update();		

		$stmt=$cc->getConnection()->prepare($sql);
        $stmt->execute($parametro);
	}
	public function delete(){
		$parametro=array();
		$cc = Singleton::getInstance();
		$sql="DELETE FROM film where film_id = :film_id";
		$parametro[":film_id"]=$this->getFilm_id();
		$stmt = $cc->getConnection()->prepare($sql);
		$stmt->execute($parametro);
	}
	public static function num_rows(){
		$cc = Singleton::getInstance();
		$sql="SELECT COUNT(*) FROM film WHERE true";
		$stmt = $cc->getConnection()->prepare($sql);
		$stmt->execute();
		$result=$stmt->fetch();
		$num_rows=$result[0];
		return $num_rows;
	}
	public static function paginas($rows_per_page){
		$num_rows=Film::num_rows();
		$paginas=ceil($num_rows/$rows_per_page);
		return $paginas;
	}
	public function list($rows_per_page, $page){
		$num_rows=Film::num_rows();
		$paginas=Film::paginas($rows_per_page);
		$cc = Singleton::getInstance();
		//Tabla de registros
		$sql="SELECT * FROM film WHERE true";
		$sql.=" LIMIT ".($rows_per_page*($page-1)).",".$rows_per_page;
		$stmt = $cc->getConnection()->prepare($sql);
		$stmt->execute();
		$result=$stmt->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}
	public function select($film_id){
		$parametro=array();
		$cc = Singleton::getInstance();
		$sql="SELECT * FROM film WHERE film_id= :film_id";
		$parametro[":film_id"]=$film_id;
		$stmt = $cc->getConnection()->prepare($sql);
		$stmt->execute($parametro);
		$result=$stmt->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}
 }